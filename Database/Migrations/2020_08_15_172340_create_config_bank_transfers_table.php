<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigBankTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_bank_transfers', function (Blueprint $table) {
            $table->id();
            $table->string("bank_wire_reservation_days")->default('3 Jours');
            $table->string('bank_wire_custom_text')->nullable();
            $table->boolean("bank_wire_payment_invite")->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_bank_transfers');
    }
}
